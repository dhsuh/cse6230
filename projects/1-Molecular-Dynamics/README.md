# Project 1: Molecular Dynamics

**Due Monday, September 18**

**Your repo transfer request** (name format `cse6230fa17-proj1-gtusername` must be submitted by **midnight on that day**

## Rules

- Write a shared memory parallel code for Brownian dynamics simulation (with Gaussian random variables as currently demonstrated in `bd.c`) with steric (the type of forces in exercise 04) interactions.  All constants should be as currently defined in the files in this directory.  If the problem is under-defined, I will update as soon as I am made aware.
    * Particle radius `a=1`
    * Time step `dt=1.e-4`
    * Mass `M=1.` (can be left out)
    * Repulsive coefficient `krepul=100.`
    * Periodic domain width `L` is problem dependent. Periodicity should be handled as it is currently handled in `interactions.c`:
        * For the purposes of computing interactions, the distance between particles is defined by being equivalent to `remainder(pi - pj,L)` for each component
        * For the purposes of computing an accurate diffusion coefficient, *periodicity is not enforced on the position itself*
- Results should be output in the *xyz* file format from Prof. Chow's lecture 7
    * Every 1 interval = 1000 time steps, write a *frame* to the output file
    * The number of intervals is problem dependent
    * The starting positions are problem dependent
- Main effort will be to parallelize the interactions function and to compute the repulsive forces in parallel
- Instructors will evaluate performance:
    * Checking out your repo on Deepthought
    * In a batch job, our script will:
        * `cd` to the top directory of your repo (`master` branch)
        * run `make INFILE=aaa.xyz OUTFILE=bbb.xyz NINTERVALS=N CSE6230UTILSDIR=/dir/dir/dir`
        * time this call
        * run the output trajectories `bbb.xyz` through a script like
          `a_msd3.m` in this directory to compute the diffusion coefficient of
          your results

## Grading

- 0-3 points for hassle-free usage: maximized if your code runs the first time from the `make` command in our script
    * Points lost if we have to figure out how to get the output we want
- 0-6 points for a parallel implementation that produces the correct results
    * Correctness is checked by computing the diffusion coefficient
    * Points will be deducted for incomprehensible code: optimized code is complicated, but explainable (with comments!)
        * Code is an intellectual product
- 0-6 Points for speed:
    * fastest code guaranteed 6
    * 0 points for as bad or worse than what I can achieve without parallelizing `interactions`
- 0-3 points for report (`proj1.pdf`, checked into repository)
    * graph of time per timestep for 10000 particles at 20 percent volume
      fraction (use logarithmic y-axis) vs. number of threads
        * plausible reproducibility: save the jobscripts that generated the
          figure, report the *version of your code (git commit)* used
    * describe your final parallelization and why you consider your parallelization to be faster than other choices


