#!/usr/bin/env python
#!/usr/bin/env python

def get_statistics(P,schedule):
    import subprocess as sub
    import os

    command = ['make','runex03','NTHREADS='+str(P),'SCHEDULE='+schedule]
    make_process = sub.Popen(' '.join(command),shell=True,stdout=sub.PIPE,stderr = sub.PIPE)
    out, err = make_process.communicate()
    outwords = out.split()
    avgs = []
    time = 0.
    for i in range(len(outwords)):
        if outwords[i] == 'steps:':
            avgs.append(float(outwords[i + 1]))
        if outwords[i] == 'seconds':
            time = float(outwords[i-1])

    return (time,avgs)

import matplotlib.pyplot as plt
import matplotlib.lines  as lines
import numpy as np

fig1 = plt.figure()
ax1  = fig1.gca()
ax1.set_title("Average Distance")
ax1.set_xlabel("Timesteps")
fig2 = plt.figure()
ax2  = fig2.gca()
ax2.set_title("Runtimes")
ax2.set_yscale("log")
ax2.set_xlabel("Threads")
ax2.set_ylabel("seconds")

for schedule in ['static,1','static,8','static,64','static','dynamic','guided']:
    times = []
    procranges = range(1,5)
    for P in procranges:
        time, avgs = get_statistics(P,schedule)
        times.append(time)
        ax1.plot([500 * i for i in range(11)],avgs,'+')
    ax2.plot(procranges,times,label=schedule)

ax2.legend()
plt.show()
